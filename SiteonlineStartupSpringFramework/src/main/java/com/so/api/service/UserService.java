package com.so.api.service;

import com.so.api.domain.User;


public interface UserService {

	User findById(int id);
	
	User findByUsername(String username);
	
	int saveUser(User user);
	
}