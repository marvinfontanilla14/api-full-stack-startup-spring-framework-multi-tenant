package com.so.api.service;

import java.util.List;

import com.so.api.domain.Cart;

public interface CartService {

	int saveCart(Cart cart);

	List<Cart> getCartListBySessionId(String sessionId);

	void deleteCart(Cart cart);	

	Cart getCartBySessionCourseInstanceId(Cart cart);
	
	void deleteCartPreviousTransDate();

}
