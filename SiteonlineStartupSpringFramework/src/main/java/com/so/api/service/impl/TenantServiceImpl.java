package com.so.api.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.so.api.dao.TenantDao;
import com.so.api.domain.Tenant;
import com.so.api.service.TenantService;

@Service("tenantService")
public class TenantServiceImpl implements TenantService {

	@Autowired
	private TenantDao tenantDao;

	@Override
	public List<Tenant> tenantList() {
		return tenantDao.tenantList();
	}

}
