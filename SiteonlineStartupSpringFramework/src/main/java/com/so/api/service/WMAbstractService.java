package com.so.api.service;

import java.util.Map;

public interface WMAbstractService {

	String sendGet(String url,String params,Map<String,String> header);
	
	String sendPost(String url,String params,Map<String,String> header);
	
	String sendPut(String url,String params,Map<String,String> header);
	
	String sendPostJson(String url,String params,Map<String,String> header);
	
}
