package com.so.api.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.so.api.dao.CartDao;
import com.so.api.domain.Cart;
import com.so.api.service.CartService;

@Service("cartService")
@Transactional
public class CartServiceImpl implements CartService{

	@Autowired
	private CartDao cartDao;

	@Override
	public int saveCart(Cart cart) {
		return cartDao.saveCart(cart);
	}

	@Override
	public List<Cart> getCartListBySessionId(String sessionId) {
		return cartDao.getCartListBySessionId(sessionId);
	}

	@Override
	public void deleteCart(Cart cart) {
		cartDao.deleteCart(cart);
	}

	@Override
	public Cart getCartBySessionCourseInstanceId(Cart cart) {
		return cartDao.getCartBySessionCourseInstanceId(cart);
	}

	@Override
	public void deleteCartPreviousTransDate() {
		cartDao.deleteCartPreviousTransDate();
	}

	
}
