package com.so.api.service;

import java.util.List;

import com.so.api.domain.Tenant;

public interface TenantService {

	List<Tenant> tenantList();

}
