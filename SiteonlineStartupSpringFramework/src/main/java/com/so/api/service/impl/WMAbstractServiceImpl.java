package com.so.api.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.so.api.dao.WMAbstractDao;
import com.so.api.service.WMAbstractService;

@Service("wMAbstractService")
//@Transactional -- For hibernate transaction only
public class WMAbstractServiceImpl implements WMAbstractService {

	@Autowired
	WMAbstractDao abstractDao;
	
	public String sendGet(String url, String params, Map<String, String> header) {
		return abstractDao.sendGet(url, params, header);
	}

	public String sendPost(String url, String params, Map<String, String> header) {
		return abstractDao.sendPost(url, params, header);
	}

	@Override
	public String sendPut(String url, String params, Map<String, String> header) {
		return abstractDao.sendPut(url, params, header);
	}

	@Override
	public String sendPostJson(String url, String params,
			Map<String, String> header) {
		return abstractDao.sendPostJson(url, params, header);
	}

}
