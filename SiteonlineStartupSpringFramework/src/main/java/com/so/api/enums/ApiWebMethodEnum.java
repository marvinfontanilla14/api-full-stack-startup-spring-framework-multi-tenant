package com.so.api.enums;


public enum ApiWebMethodEnum {

	// -- aXcelerate Web Method prefix:AX
	
	// -- courses
	AX_COURSES("/courses/",WebMethodRequestType.REQUEST_GET.getType()),
	AX_COURSE_DETAIL("/course/detail",WebMethodRequestType.REQUEST_GET.getType()),
	AX_COURSE_INSTANCES("/course/instances",WebMethodRequestType.REQUEST_GET.getType()),
	AX_COURSE_INSTANCE_ATTENDANCE("/course/instance/attendance",WebMethodRequestType.REQUEST_GET.getType()),
	AX_COURSE_INSTANCE_SEARCH("/course/instance/search",WebMethodRequestType.REQUEST_POST.getType()),
	AX_COURSE_INSTANCE_DETAIL("/course/instance/detail",WebMethodRequestType.REQUEST_GET.getType()),
	AX_COURSE_INSTANCE("/course/instance/",WebMethodRequestType.REQUEST_POST.getType()),
	AX_COURSE_ENROL("/course/enrol",WebMethodRequestType.REQUEST_POST.getType()),
	AX_COURSE_ENROL_MULTIPLE("/course/enrolMultiple",WebMethodRequestType.REQUEST_POST.getType()),
	AX_COURSE_ENROLMENTS("/course/enrolments",WebMethodRequestType.REQUEST_GET.getType()),
	AX_COURSE_ENROLMENT("/course/enrolment",WebMethodRequestType.REQUEST_PUT.getType()),
	AX_COURSE_DISCOUNTS("/course/discounts",WebMethodRequestType.REQUEST_GET.getType()),
	AX_COURSE_ENQUIRE("/course/enquire",WebMethodRequestType.REQUEST_POST.getType()),
	AX_COURSE_CALENDAR("/course/calendar",WebMethodRequestType.REQUEST_GET.getType()),
	AX_COURSE_LOCATIONS("/course/locations",WebMethodRequestType.REQUEST_GET.getType()),
	AX_COURSE_RESOURCES("/course/resources",WebMethodRequestType.REQUEST_GET.getType()),
	
	//-- contact
	AX_CONTACT("/contact/",WebMethodRequestType.REQUEST_POST.getType()),
	AX_CONTACTS("/contacts/",WebMethodRequestType.REQUEST_GET.getType()),
	AX_CONTACTS_SEARCH("/contacts/search",WebMethodRequestType.REQUEST_GET.getType()),
	AX_CONTACT_ENROLMENTS("/contact/enrolments/",WebMethodRequestType.REQUEST_GET.getType()),
	AX_CONTACT_NOTES("/contact/notes/",WebMethodRequestType.REQUEST_GET.getType()),
	
	// -- REPORT
	AX_SAVED_REPORT("/savedReport/",WebMethodRequestType.REQUEST_GET.getType()),
	
	//-- trainer
	
	AX_TRAINER("/trainer/",WebMethodRequestType.REQUEST_GET.getType()),
	
	//-- accounting
	AX_ACCOUNTING_INVOICE("/accounting/invoice/",WebMethodRequestType.REQUEST_PUT.getType()),
	AX_ACCOUNTING_TRANSACTION("/accounting/transaction/",WebMethodRequestType.REQUEST_POST.getType()),
	
	
	//-- task
	AX_GET_TASK_CATEGORY("/task/category",WebMethodRequestType.REQUEST_POST.getType()),
	
	// -- skill group
	AX_SKILL_GROUP("/skillGroup/",WebMethodRequestType.REQUEST_POST.getType()),
	
	AX_USER_LOGIN("/user/login",WebMethodRequestType.REQUEST_POST.getType()),
	
	// -- End of aXcelerate Web Method
	
	
	// -- Moodle Web Method prefix:MD
	
	MD_MERIDA_COURSE_GET_COURSE_BY_FIELD("merida_course_get_courses_by_field",WebMethodRequestType.REQUEST_GET.getType()),
	MERIDA_COURSE_GET_ACTIVITY_COMPLETION_BY_FIELD("merida_course_get_activity_completion_by_field",WebMethodRequestType.REQUEST_GET.getType()),
	MERIDA_COURSE_GET_COURSE_COMPLETION_BY_FIELD("merida_course_get_course_completion_by_field",WebMethodRequestType.REQUEST_GET.getType()),
	MERIDA_GET_USERS_BY_FIELD("merida_get_users_by_field",WebMethodRequestType.REQUEST_GET.getType()),
	MD_CORE_USER_GET_USERS_BY_FIELD("core_user_get_users_by_field",WebMethodRequestType.REQUEST_GET.getType()),
	MD_MERIDA_ENROL_USER_CERTIFICATES("merida_enrol_user_certificates",WebMethodRequestType.REQUEST_GET.getType()),
	
	// -- END of Moodle Web Method

	;
	
	private String method;
	private String requestType;
	
	private ApiWebMethodEnum(String method, String requestType){
		setMethod(method);
		setRequestType(requestType);
	}

	public String getMethod() {
		return method;
	}

	private void setMethod(String method) {
		this.method = method;
	}

	public String getRequestType() {
		return requestType;
	}

	private void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	
}
