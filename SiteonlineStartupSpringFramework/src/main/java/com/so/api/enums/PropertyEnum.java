package com.so.api.enums;

public enum PropertyEnum {
	
	DATE_TIME_FORMAT("yyyy-MM-dd'T'HH:mm:ss.SSSZ"),
	
	// -- Get Property value
	AX_ENTRY_POINT("ax.entry.point"),
	AX_API_TOKEN("ax.apitoken"),
	AX_WS_TOKEN("ax.wstoken"),
	
	// -- AX HEADER PARAM
	AX_API_TOKEN_PARAM("apitoken"),
	AX_WS_TOKEN_PARAM("wstoken"),
	
	MD_ENTRY_POINT("md.entry.point"),
	MD_WS_TOKEN("md.wstoken"),
	MD_REST_FORMAT("json"),
	
	//-- Moodle Default Param
	MD_WS_TOKEN_PARAM("wstoken"),
	MD_REST_FORMAT_PARAM("moodlewsrestformat"),
	MD_WS_FUNCTION("wsfunction"),
	
	//-- API Type
	API_TYPE_AX("Axcelerate"),
	API_TYPE_MOODLE("Moodle")
	;
	
	private String val;

	private PropertyEnum(String val) {
		setVal(val);
	}

	public String getVal(){
		return this.val;
	}
	
	private void setVal(String val){
		this.val = val;
	}
	
}
