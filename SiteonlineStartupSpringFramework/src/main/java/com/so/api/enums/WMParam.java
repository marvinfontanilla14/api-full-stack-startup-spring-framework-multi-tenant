package com.so.api.enums;

public class WMParam {

	public enum AX_COURSES {
		
		SEARCH_TERM("searchTerm"),
		TYPE("type"),
		TRAINING_AREA("trainingArea"),
		OFFSET("offset"),
		DISPLAY_LENGTH("displayLength"),
		SORT_COLUMN("sortColumn"),
		SORT_DIRECTION("sortDirection"),
		CURRENT("current"),
		PUBLIC("public"),
		;
		
		private String val;
		
		private AX_COURSES(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum AX_COURSE_DETAIL {
		
		ID("ID"),
		TYPE("type")
		;
		
		private String val;
		
		private AX_COURSE_DETAIL(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum AX_TASK_CATEGORY {
		
		CATEGORY_ID("categoryID"),
		NAME("name"),
		COLOUR("colour"),
		;
		
		private String val;
		
		private AX_TASK_CATEGORY(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum AX_SKILL_GROUP {
		
		NAME("name"),
		TYPE("type"),
		;
		
		private String val;
		
		private AX_SKILL_GROUP(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum AX_USER_LOGIN {
		
		USERNAME("username"),
		PASSWORD("password"),
		;
		
		private String val;
		
		private AX_USER_LOGIN(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum MD_MERIDA_COURSE_GET_COURSE_BY_FIELD {
		
		FIELD("field"),
		VALUE("value"),
		ID("id"),
		CATEGORY("category"),
		SHORTNAME("shortname"),
		FULLNAME("fullname")
		;
		
		private String val;
		
		private MD_MERIDA_COURSE_GET_COURSE_BY_FIELD(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum MERIDA_COURSE_GET_ACTIVITY_COMPLETION_BY_FIELD{
		
		STUDENT("student"),
		COURSE("course"),
		MODULES("modules")
		;
		
		private String val;
		
		private MERIDA_COURSE_GET_ACTIVITY_COMPLETION_BY_FIELD(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
		
	}
	
	
	public enum MERIDA_GET_USERS_BY_FIELD{
		
		FIELD("field"),
		VALUES("values")
		;
		
		private String val;
		
		private MERIDA_GET_USERS_BY_FIELD(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
		
	}
	
	
	public enum MERIDA_COURSE_GET_COURSE_COMPLETION_BY_FIELD{
		
		STUDENT("student")
		;
		
		private String val;
		
		private MERIDA_COURSE_GET_COURSE_COMPLETION_BY_FIELD(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
		
	}
	
	public enum MD_CORE_USER_GET_USERS_BY_FIELD {
		
		FIELD("field"),
		VALUES("values"),
		ID("id"),
		IDNUMBER("idnumber"),
		USERNAME("username"),
		EMAIL("email")
		;
		
		private String val;
		
		private MD_CORE_USER_GET_USERS_BY_FIELD(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum AX_COURSE_INSTANCES {
		
		ID("ID"),
		TYPE("type"),
		PUBLIC("public"),
		CURRENT("current"),
		;
		
		private String val;
		
		private AX_COURSE_INSTANCES(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum AX_COURSE_ENROLMENTS {
		
		ID("ID")
		;
		
		private String val;
		
		private AX_COURSE_ENROLMENTS(String val){
			setVal(val);
		}
		
		public String getVal() {
			return val;
		}
		
		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum AX_CONTACT {
		
		GIVEN_NAME("givenName"),
		SURNAME("surname"),
		TITLE("title"),
		EMAIL_ADDRESS("emailAddress"),
		DOB("dob"),
		USI("usi"),
		SEX("sex"),
		PHONE("phone"),
		DISPLAY_LENGTH("displayLength"),
		ORGANISATION("organisation"),
		CITIZEN_STATUS_NAME("citizenStatusName"),
		COUNTRY_OF_BIRTH_NAME("countryOfBirthName"),
		INDIGENOUS_STATUS_NAME("indigenousStatusName"),
		COUNTRY_OF_CITIZEN_NAME("countryOfCitizenName"),
		EMERGENCY_CONTACT("emergencyContact"),
		LABOUR_FORCE_NAME("labourForceName"),
		MAIN_LANGUAGE_NAME("mainLanguageName"),
		ENGLISH_PROFICIENCY_ID("englishProficiencyId"),
		ENGLISH_ASSISTANCE_FLAG("englishAssistanceFlag"),
		HIGHEST_SCHOOL_LEVEL_ID("highestSchoolLevelId"),
		AT_SCHOOL_FLAG("atSchoolFlag"),
		HIGHEST_SCHOOL_LEVEL_YEAR("highestSchoolLevelYear"),
		PRIOR_EDUCATION_NAMES("priorEducationNames"),
		ADDRESS1("address1"),
		MIDDLE_NAME("middleName"),
		MOBILE_PHONE("mobilephone"),
		WORK_PHONE("workphone"),
		FAX("fax"),
		POSITION("position"),
		SECTION("section"),
		DIVISION("division"),
		SOURCE_CODE_ID("SourceCodeID"),
		PASSWORD("Password"),
		LUI("LUI"),
		EMERGENCY_CONTACT_RELATION("EmergencyContactRelation"),
		EMERGENCY_CONTACT_PHONE("EmergencyContactPhone"),
		BUILDING_NAME("buildingName"),
		UNIT_NO("unitNo"),
		STREET_NO("streetNo"),
		STREET_NAME("streetName"),
		PO_BOX("POBox"),
		ADDRESS2("address2"),
		CITY("city"),
		STATE("state"),
		POSTCODE("postcode"),
		COUNTRY_ID("countryID"),
		COUNTRY("country"),
		S_BUILDING_NAME("sbuildingName"),
		S_UNIT_NO("sunitNo"),
		S_STREET_NO("sstreetNo"),
		S_STREET_NAME("sstreetName"),
		S_PO_BOX("sPOBox"),
		S_ADDRESS1("saddress1"),
		S_ADDRESS2("saddress2"),
		S_CITY("scity"),
		S_STATE("sstate"),
		S_POSTCODE("spostcode"),
		S_COUNTRY_ID("scountryID"),
		S_COUNTRY("scountry"),
		COUNTRY_OF_BIRTH_ID("CountryofBirthID"),
		CITY_OF_BIRTH("CityofBirth"),
		COUNTRY_OF_CITIZEN_ID("CountryofCitizenID"),
		CITIZEN_STATUS_ID("CitizenStatusID"),
		LABOUR_FORCE_ID("LabourForceID"),
		MAIN_LANGUAGE_ID("MainLanguageID"),
		AT_SCHOOL_NAME("AtSchoolName"),
		PRIOR_EDUCATION_STATUS("PriorEducationStatus"),
		PRIOR_EDUCATION_IDS("PriorEducationIDs"),
		DISABILITY_FLAG("DisabilityFlag"),
		DISABILITY_TYPE_IDS("DisabilityTypeIDs"),
		INDIGENOUS_STATUS_ID("IndigenousStatusID"),
		ANZSCO_CODE("ANZSCOCode"),
		ANZSIC_CODE("ANZSICCode"),
		EMPLOYER_CONTACT_ID("employerContactID"),
		PAYER_CONTACT_ID("payerContactID"),
		SUPERVISOR_CONTACT_ID("supervisorContactID"),
		AGENT_CONTACT_ID("agentContactID"),
		COACH_CONTACT_ID("coachContactID"),
		INTERNATIONAL_CONTACT_ID("internationalContactID"),
		OPTIONAL_ID("optionalID"),
		;
		
		private String val;
		
		private AX_CONTACT(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum AX_CONTACTS_SEARCH {
		
		Q("q"),
		OFFSET("offset"),
		DISPLAY_LENGTH("displayLength"),
		GIVEN_NAME("givenName"),
		SURNAME("surname"),
		TITLE("title"),
		EMAIL_ADDRESS("emailAddress"),
		DOB("dob"),
		PHONE("phone"),
		ADDRESS1("address1")	
		;
		
		private String val;
		
		private AX_CONTACTS_SEARCH(String val){
			setVal(val);
		}
		
		public String getVal() {
			return val;
		}
		
		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum AX_CONTACT_ENROLMENTS {
		
		CONTACT_ID("contactID"),
		TYPE("type")
		;
		
		private String val;
		
		private AX_CONTACT_ENROLMENTS(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum AX_COURSE_ENROL {
		
		CONTACT_ID("contactID"),
		INSTANCE_ID("instanceID"),
		TYPE("type"),
		PAYER_ID("payerID"),
		;
		
		private String val;
		
		private AX_COURSE_ENROL(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum AX_COURSE_ENROLMENT {
		
		CONTACT_ID("contactID"),
		INSTANCE_ID("instanceID"),
		TYPE("type"),
		LOG_TYPE("logType"),
		ENROLMENT_STATUS("enrolmentStatus"),
		;
		
		private String val;
		
		private AX_COURSE_ENROLMENT(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum AX_TRAINER {
		
		CONTACT_ID("contactId")
		;
		
		private String val;
		
		private AX_TRAINER(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum AX_ACCOUNTING_INVOICE {
		
		LOCK("lock"),
		;
		
		private String val;
		
		private AX_ACCOUNTING_INVOICE(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum AX_ACCOUNTING_TRANSACTION {
		
		AMOUNT("amount"),
		CONTACT_ID("contactID"),
		INVOICE_ID("invoiceID"),
		PAYMENT_METHOD_ID("paymentMethodID"),
		REFERENCE("reference"),
		BANK_NAME("BankName"),
		;
		
		private String val;
		
		private AX_ACCOUNTING_TRANSACTION(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum AX_COURSE_INSTANCE_DETAIL {
		
		INSTANCE_ID("instanceID"),
		TYPE("type"),
		;
		
		private String val;
		
		private AX_COURSE_INSTANCE_DETAIL(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum AX_SAVED_REPORT {
		
		FILTER_OVERRIDE("filterOverride")
		;
		
		private String val;
		
		private AX_SAVED_REPORT(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
	public enum MD_MERIDA_ENROL_USER_CERTIFICATES {
		
		USER_ID("userid"),
		;
		
		private String val;
		
		private MD_MERIDA_ENROL_USER_CERTIFICATES(String val){
			setVal(val);
		}

		public String getVal() {
			return val;
		}

		public void setVal(String val) {
			this.val = val;
		}
		
	}
	
}
