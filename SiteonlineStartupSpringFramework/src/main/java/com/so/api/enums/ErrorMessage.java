package com.so.api.enums;

public enum ErrorMessage {

	INVALID_FIELD_VALUE("Invalid field value"),
	// -- Invalid field message
	INVALID_EMAIL("Invalid email"),
	INVALID_NUMBER("Invalid number"),
	
	//-- Required field message
	REQUIRED_USERNAME("username is required"),
	REQUIRED_PASSWORD("password is required"),
	REQUIRED_EMAIL("email is required"),
	REQUIRED_FIRST_NAME("firstName is required"),
	REQUIRED_LAST_NAME("lastName is required"),
	REQUIRED_TENANT("tenant is required"),
	
	
	;
	
	private String errorMgs;

	public String getErrorMgs() {
		return errorMgs;
	}

	public void setErrorMgs(String errorMgs) {
		this.errorMgs = errorMgs;
	}

	private ErrorMessage(String val) {
		setErrorMgs(val);
	}

	
}
