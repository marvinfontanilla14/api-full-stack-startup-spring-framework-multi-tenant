package com.so.api.enums;

public enum UserProfileType {
	USER("USER", 1),
	ADMIN("ADMIN", 2),
	DBA("DBA", 3),
	
	;
	String userProfileType;
	int userProfileTypeId;
	
	private UserProfileType(String userProfileType, int userProfileTypeId){
		this.userProfileType = userProfileType;
		this.userProfileTypeId = userProfileTypeId;
	}
	
	public String getUserProfileType(){
		return userProfileType;
	}

	public int getUserProfileTypeId(){
		return userProfileTypeId;
	}
	
}
