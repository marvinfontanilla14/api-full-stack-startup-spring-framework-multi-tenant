package com.so.api.enums;

public enum WebMethodRequestType {

	REQUEST_POST("POST"),
	REQUEST_GET("GET"),
	REQUEST_PUT("PUT"),
	REQUEST_DELETE("DELETE"),
	;
	
	private String type;
	
	private WebMethodRequestType(String type){
		setType(type);
	}

	public String getType() {
		return type;
	}

	private void setType(String type) {
		this.type = type;
	}
	
}
