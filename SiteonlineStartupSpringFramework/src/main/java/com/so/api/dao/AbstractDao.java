package com.so.api.dao;

import java.io.Serializable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractDao {

	@Autowired
	private SessionFactory sessionFactory;

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}
	
	public void persist(Object entity) {
		getSession().persist(entity);
	}

	public void delete(Object entity) {
		getSession().delete(entity);
	}

	public void update(Object entity) {
		getSession().update(entity);
	}

	public void save(Object entity) {
		getSession().save(entity);
	}
	
	public void clearSession(){
		getSession().clear();
	}
	
	public int saveAndReturnId(Object entity) {
		int result = -1;
		
		try {
			Serializable ser = getSession().save(entity);
	        if (ser != null) {
	            result = (Integer) ser;
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		return result;
	}
}
