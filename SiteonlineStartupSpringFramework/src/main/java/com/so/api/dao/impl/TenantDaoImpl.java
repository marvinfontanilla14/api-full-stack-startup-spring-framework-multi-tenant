package com.so.api.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.so.api.dao.TenantDao;
import com.so.api.domain.Tenant;
import com.so.api.utilities.JsonBeanParser;
import com.so.api.utilities.SOUtil;

@Repository("tenantDao")
public class TenantDaoImpl implements TenantDao{

	@Override
	public List<Tenant> tenantList() {
		String jsonString = SOUtil.readResourceFile("tenantConfig.json");
		JsonBeanParser<Tenant> jup = new JsonBeanParser<Tenant>();
		return jup.parseJsonListToBean(List.class, Tenant.class, jsonString);
	}

	
}
