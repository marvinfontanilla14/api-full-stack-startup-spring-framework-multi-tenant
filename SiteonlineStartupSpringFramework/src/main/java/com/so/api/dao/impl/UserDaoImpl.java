package com.so.api.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.so.api.dao.AbstractDao;
import com.so.api.dao.UserDao;
import com.so.api.domain.User;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao implements UserDao {

	public User findById(int id) {
		Criteria criteria = getSession().createCriteria(User.class)
				.add(Restrictions.eq("id", id));
		
		if (criteria.uniqueResult() == null) {
			return new User();
		}
		
		return (User) criteria.uniqueResult();
	}

	public User findByUsername(String username) {
		Criteria criteria = getSession().createCriteria(User.class)
				.add(Restrictions.eq("username", username));
		
		if (criteria.uniqueResult() == null) {
			return new User();
		}
		
		return (User) criteria.uniqueResult();
	}

	@Override
	public int saveUser(User user) {
		return saveAndReturnId(user);
	}

	
}
