package com.so.api.dao;

import java.util.List;

import com.so.api.domain.Cart;

public interface CartDao {

	int saveCart(Cart cart);

	List<Cart> getCartListBySessionId(String sessionId);

	void deleteCart(Cart cart);	

	Cart getCartBySessionCourseInstanceId(Cart cart);

	void deleteCartPreviousTransDate();
	
}
