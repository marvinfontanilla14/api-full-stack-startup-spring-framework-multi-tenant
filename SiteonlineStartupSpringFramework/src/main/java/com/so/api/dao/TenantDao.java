package com.so.api.dao;

import java.util.List;

import com.so.api.domain.Tenant;

public interface TenantDao {

	List<Tenant> tenantList();

}
