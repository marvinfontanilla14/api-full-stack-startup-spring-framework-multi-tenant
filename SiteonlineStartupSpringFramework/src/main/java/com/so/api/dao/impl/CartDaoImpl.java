package com.so.api.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;

import com.so.api.dao.AbstractDao;
import com.so.api.dao.CartDao;
import com.so.api.domain.Cart;

@Repository("cartDao")
public class CartDaoImpl extends AbstractDao implements CartDao {

	@Override
	public int saveCart(Cart cart) {
		return saveAndReturnId(cart);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Cart> getCartListBySessionId(String sessionId) {
		Criteria criteria = getSession().createCriteria(Cart.class)
				.add(Restrictions.eq("sessionId", sessionId));
		return (List<Cart>) criteria.list();
	}

	@Override
	public void deleteCart(Cart cart) {
		delete(cart);
	}

	@Override
	public Cart getCartBySessionCourseInstanceId(Cart cart) {
		Criteria criteria = getSession().createCriteria(Cart.class)
				.add(Restrictions.eq("sessionId", cart.getSessionId()))
				.add(Restrictions.eq("courseId", cart.getCourseId()))
				.add(Restrictions.eq("instanceId", cart.getInstanceId()))
				.add(Restrictions.eq("type", cart.getType()));
		return (Cart) criteria.uniqueResult();
	}

	@Override
	public void deleteCartPreviousTransDate() {
		int days = 2;
		Date daysAgo = new DateTime(new Date()).minusDays(days).toDate();
		Query query = getSession().createQuery("delete Cart where dateCreated<=:dateCreated");
		query.setTimestamp("dateCreated", daysAgo);
		query.executeUpdate();
	}

}
