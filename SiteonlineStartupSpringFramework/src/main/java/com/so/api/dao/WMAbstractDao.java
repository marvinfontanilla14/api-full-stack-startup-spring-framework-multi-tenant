package com.so.api.dao;

import java.util.Map;

public interface WMAbstractDao {

	String sendGet(String url,String params,Map<String,String> header);
	
	String sendPost(String url,String params,Map<String,String> header);
	
	String sendPut(String url,String params,Map<String,String> header);
	
	String sendPostJson(String url,String params,Map<String,String> header);
	
}
