package com.so.api.dao.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.so.api.dao.WMAbstractDao;
import com.so.api.utilities.HttpURLConn;

@Repository("wMAbstractDao")
public class WMAbstractDaoImpl implements WMAbstractDao {

	public String sendGet(String url, String params, Map<String,String> header) {
		String result = "";
		try {
			result =  HttpURLConn.doGet(url,params,header);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public String sendPost(String url, String params, Map<String,String> header) {
		String result = "";
		try {
			result =  HttpURLConn.doPost(url,params,header);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String sendPut(String url, String params, Map<String, String> header) {
		String result = "";
		try {
			result =  HttpURLConn.doPut(url,params,header);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String sendPostJson(String url, String params,
			Map<String, String> header) {
		String result = "";
		try {
			result =  HttpURLConn.doPostJson(url,params,header);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
