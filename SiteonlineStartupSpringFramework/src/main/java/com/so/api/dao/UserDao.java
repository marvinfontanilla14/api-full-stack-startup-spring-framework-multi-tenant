package com.so.api.dao;

import com.so.api.domain.User;


public interface UserDao {

	User findById(int id);
	
	User findByUsername(String username);
	
	int saveUser(User user);
	
}

