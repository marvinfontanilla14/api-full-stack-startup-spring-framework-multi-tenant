package com.so.api.configuration;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletContext;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;
import org.springframework.web.context.support.ServletContextResource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.XmlViewResolver;

import com.so.api.handler.resolver.adapter.MultiTenancyInterceptor;
import com.so.api.service.UserService;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.so.api")
public class AppConfiguration extends WebMvcConfigurerAdapter {

	@Autowired
	ServletContext servletContext;

	@Autowired
	private OAuth2SecurityConfiguration securityConfig;

	@Autowired
	private UserService userService;

	@Bean
	public MessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("messages");
		return messageSource;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/config/**")
				.addResourceLocations("classpath:/WEB-INF/config/")
				.setCachePeriod(31556926);
		registry.addResourceHandler("/resources/**")
				.addResourceLocations("/resources/").setCachePeriod(31556926);
	}

	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	@Bean
	public XmlViewResolver getXmlViewResolver() {
		XmlViewResolver resolver = new XmlViewResolver();
		resolver.setLocation(new ServletContextResource(servletContext,
				"/WEB-INF/config/jasper-view.xml"));
		resolver.setOrder(1);
		return resolver;
	}

	@Bean
	public VelocityEngine velocityEngine() throws VelocityException,
			IOException {
		VelocityEngineFactoryBean factory = new VelocityEngineFactoryBean();
		Properties props = new Properties();
		props.put("resource.loader", "class");
		props.put("class.resource.loader.class",
				"org.apache.velocity.runtime.resource.loader."
						+ "ClasspathResourceLoader");
		factory.setVelocityProperties(props);

		return factory.createVelocityEngine();
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	// -- Pre Request Handler
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new MultiTenancyInterceptor(userService));
	}
}
