package com.so.api.configuration;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
@PropertySource("classpath:mail.properties")
public class MailConfiguration {

	@Autowired
	private Environment environment;

	@Bean
	public JavaMailSender javaMailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		Properties mailProperties = new Properties();
		mailProperties.put("mail.smtp.auth", environment.getRequiredProperty("mail.smtp.auth"));
		mailProperties.put("mail.smtp.starttls.enable", environment.getRequiredProperty("mail.smtp.starttls.enable"));
		mailSender.setJavaMailProperties(mailProperties);
		mailSender.setHost(environment.getRequiredProperty("mail.host"));
		mailSender.setPort(Integer.parseInt(environment.getRequiredProperty("mail.port")));
		mailSender.setProtocol(environment.getRequiredProperty("mail.protocol"));
		mailSender.setUsername(environment.getRequiredProperty("mail.username"));
		mailSender.setPassword(environment.getRequiredProperty("mail.password"));
		return mailSender;
	}

}
