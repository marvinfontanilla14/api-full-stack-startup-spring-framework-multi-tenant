package com.so.api.handler.resolver.adapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.so.api.domain.User;
import com.so.api.service.UserService;

public class MultiTenancyInterceptor extends HandlerInterceptorAdapter {

	private UserService userService;
    
    public MultiTenancyInterceptor(UserService userService) {
    	this.userService = userService;
    }
	
	@Override
	public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object handler)
			throws Exception {
		
		User user =  userService.findByUsername(getPrincipal());
		req.setAttribute("CURRENT_TENANT_IDENTIFIER", user.getTenant());

		return true;
	}
	
	private String getPrincipal(){
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			userName = ((UserDetails)principal).getUsername();
		} else {
			userName = principal.toString();
		}
		return userName;
	}
}
