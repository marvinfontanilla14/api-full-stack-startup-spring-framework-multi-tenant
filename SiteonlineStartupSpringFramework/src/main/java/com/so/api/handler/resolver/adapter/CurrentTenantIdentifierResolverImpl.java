package com.so.api.handler.resolver.adapter;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

@Component
@PropertySource(value = { "classpath:application.properties" })
public class CurrentTenantIdentifierResolverImpl implements CurrentTenantIdentifierResolver {

	@Autowired
	private Environment environment;
	
	@Override
	public String resolveCurrentTenantIdentifier() {
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		if (requestAttributes != null) {
			String identifier = (String) requestAttributes.getAttribute("CURRENT_TENANT_IDENTIFIER",RequestAttributes.SCOPE_REQUEST);
			if (identifier != null) {
				return identifier;
			}
		}
		return environment.getRequiredProperty("jdbc.dbName");
	}

	@Override
	public boolean validateExistingCurrentSessions() {
		return true;
	}
}
