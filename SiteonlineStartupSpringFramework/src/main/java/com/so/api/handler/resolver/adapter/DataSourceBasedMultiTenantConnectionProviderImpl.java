package com.so.api.handler.resolver.adapter;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.hibernate.engine.jdbc.connections.spi.AbstractDataSourceBasedMultiTenantConnectionProviderImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import com.so.api.domain.Tenant;
import com.so.api.service.TenantService;

@Component
@PropertySource(value = { "classpath:application.properties" })
public class DataSourceBasedMultiTenantConnectionProviderImpl extends
		AbstractDataSourceBasedMultiTenantConnectionProviderImpl {

	@Autowired
	private Environment environment;
	
	private static final long serialVersionUID = 8168907057647334460L;
	private String DEFAULT_TENANT_ID = "";

	@Autowired
	public DataSource dataSource;
	
	@Autowired
	private TenantService tenantService;

	private DataSource dataSource(Tenant tenant) {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(tenant.getClassName());
		dataSource.setUrl(tenant.getDbUrl()+"/"+tenant.getDbName());
		dataSource.setUsername(tenant.getUsername());
		dataSource.setPassword(tenant.getPassword());
		return dataSource;
	}

	private Map<String, DataSource> map;

	@PostConstruct
	public void load() {
		DEFAULT_TENANT_ID = environment.getRequiredProperty("jdbc.dbName");
		map = new HashMap<>();
		map.put(DEFAULT_TENANT_ID, dataSource);
		for(Tenant tenant : tenantService.tenantList()) {
			map.put(tenant.getDbName(),dataSource(tenant));
		}
	}

	@Override
	protected DataSource selectAnyDataSource() {
		return map.get(DEFAULT_TENANT_ID);
	}

	@Override
	protected DataSource selectDataSource(String tenantIdentifier) {
		return map.get(tenantIdentifier);
	}

}
