package com.so.api.utilities;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.apache.log4j.Logger;

	public class HttpURLConn {
		
		private static final Logger logger = Logger.getLogger(HttpURLConn.class);
		
		// HTTP GET request
		public static String doGet(String url, String params, Map<String,String> headers) throws Exception {
			logger.info("============ CALL HttpURLConn -> doGet START ===========");
			URL obj = new URL(url + params);
			
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			
			// optional default is GET
			con.setRequestMethod("GET");
			con.setDoOutput(true);
			//add request header
			for (Map.Entry<String,String> entry : headers.entrySet()) {
			    con.setRequestProperty(entry.getKey(), entry.getValue());
			}
			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			logger.info("============ CALL HttpURLConn -> doGet END ===========");
			return response.toString();
		}
		
		// HTTP POST request
		public static String doPost(String url, String params, Map<String,String> headers) throws Exception {
			logger.info("============ CALL HttpURLConn -> doPost START ===========");
			URL obj = new URL(url);
			
//			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//add reuqest header
			con.setRequestMethod("POST");
			
			//add request header
			for (Map.Entry<String,String> entry : headers.entrySet()) {
			    con.setRequestProperty(entry.getKey(), entry.getValue());
			}
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(params);
			wr.flush();
			wr.close();
			con.getResponseCode();
			con.getResponseMessage();

			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			logger.info("============ CALL HttpURLConn -> doPost END ===========");
			return response.toString();
		}
		
		// HTTP PUT request
		public static String doPut(String url, String params, Map<String,String> headers) throws Exception {
			logger.info("============ CALL HttpURLConn -> doPut START ===========");
			URL obj = new URL(url+"?"+params);
			
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("PUT");
			con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			con.setRequestProperty("Accept", "application/json");
			
			//add request header
			for (Map.Entry<String,String> entry : headers.entrySet()) {
			    con.setRequestProperty(entry.getKey(), entry.getValue());
			}
			
			// Send post request
			con.setDoOutput(true);
			con.setUseCaches (false);
			con.setDoInput(true);
			
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			logger.info("============ CALL HttpURLConn -> doPut END ===========");
			return response.toString();
		}

		public static String doPostJson(String url, String params, Map<String,String> headers) throws Exception {
			logger.info("============ CALL HttpURLConn -> doPostJson START ===========");
			URL obj = new URL(url);
			
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			
			//add request header
			for (Map.Entry<String,String> entry : headers.entrySet()) {
			    con.setRequestProperty(entry.getKey(), entry.getValue());
			}
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			con.setRequestProperty("Content-Type", "application/json");
			
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.write(params.getBytes());
			wr.flush();
			wr.close();

			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			logger.info("============ CALL HttpURLConn -> doPostJson END ===========");
			return response.toString();
		}
	}
	

