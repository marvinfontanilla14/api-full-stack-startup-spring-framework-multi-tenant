package com.so.api.utilities;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Scanner;

import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.so.api.enums.PropertyEnum;

public class SOUtil {
	


	public static ObjectMapper setDateFormat(ObjectMapper mapper) {
		DateFormat df = new SimpleDateFormat(
				PropertyEnum.DATE_TIME_FORMAT.getVal());
		mapper.setDateFormat(df);
		return mapper;
	}

	public static ObjectMapper configureFeature(ObjectMapper mapper) {
		mapper.configure(Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper;
	}

	public static String generateUrlParam(Map<String, String> params) {
		String genParams = "";
		int counter = 0;
		for (Map.Entry<String, String> entry : params.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			if(value != null) {
				counter++;
				if (counter == 1) {
					try {
						genParams += key + "=" + URLEncoder.encode(value,"UTF-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				} else {
					try {
						genParams += "&" + key + "=" + URLEncoder.encode(value,"UTF-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				}
			}

		}

		return genParams;
	}

	private String getFile(String fileName) {
		 
		StringBuilder result = new StringBuilder("");

		// Get file from resources folder
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());

		try (Scanner scanner = new Scanner(file)) {

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				result.append(line).append("\n");
			}

			scanner.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return result.toString();

	}
	
	public static String readResourceFile(String fileName) {
		return new SOUtil().getFile(fileName);
	}

	public static JSONObject getJsonObject(String objectName){
		JSONArray jsonArray = getJsonConfig();
		JSONObject jsonObject = null;

		for(Object obj : jsonArray){
			jsonObject = ((JSONObject) obj).getJSONObject(objectName);
			break;
//			jsonObject = ((JSONObject) obj).getJSONObject(company).getString("EntryPoint");
		}
		
		return jsonObject;
	}
	
	
	public static JSONArray getJsonConfig(){
		String jsonString = readResourceFile("jsonConfig.txt");
		return new JSONArray(jsonString);
	}
	
	public static String doEncrypt(String txt) {
		return new BCryptPasswordEncoder().encode(txt);
	}
	
	public static Date stringToDate(String strDate, String currFormat) {
		DateFormat df = new SimpleDateFormat(currFormat);
		Date result = null;
		
		try {
			result = df.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static String dateToString(Date date, String format) {
		DateFormat df = new SimpleDateFormat(format);
		return df.format(date);
	}
	
	public static String formatPaymentType(String type) {
		String method = "";
    	switch(type){
    	case  "creditCard" :
    		method = "Credit Card";
    		break;
    	case  "payPal" :
    		method = "Pay Pal";
    		break;
    	case  "Eft" :
    		method = "EFT";
    		break;
    	case  "phone" :
    		method = "Phone";
    		break;
    	}
    	return method;
		
	}
	
	public static String formatDecimal(Double val) {
		return String.format( "%,.2f", val );
	}
	
	  //-- from yyyy-mm-dd to mm/dd/yyyy
	  public static String formatStrDate (String strDate) {
		  if(strDate !=null && strDate.length() > 0) {
//			  2016-08-26 16:00
			  strDate = strDate.substring(0,10);
			  String [] arrDate = strDate.split("-");
			  return arrDate[1]+"/"+arrDate[2]+"/"+arrDate[0];
		  }
		  return "";
	  }
	
}
