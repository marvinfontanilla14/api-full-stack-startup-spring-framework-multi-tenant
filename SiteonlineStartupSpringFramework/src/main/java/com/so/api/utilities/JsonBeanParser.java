package com.so.api.utilities;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.JavaType;

public class JsonBeanParser<T> {

		private T t;
		private List<T> list;
		private URL url;
		private ObjectMapper mapper;
		private JavaType type;
		
		public T getT() {
			return t;
		}
	
		public void setT(T t) {
			this.t = t;
		}
		
		private static final Logger logger = Logger.getLogger(JsonBeanParser.class);

		// -- Parse nested Json from Url to array list of bean(POJO)
		@SuppressWarnings("rawtypes")
		public List<T> parseJsonListUrlToBean(Class <? extends Collection> collectionClass, Class <?> elementClass, String strUrl) {
			logger.info("============ CALL JsonBeanParser -> parseJsonListUrlToBean START ===========");
			mapper = new ObjectMapper();
			type = mapper.getTypeFactory().constructCollectionType(collectionClass, elementClass);
			mapper = SOUtil.configureFeature(mapper);
			try {
				url = new URL(strUrl);
				list = mapper.readValue(url,type);
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			logger.info("============ CALL JsonBeanParser -> parseJsonListUrlToBean END ===========");
			return list;
		}
	    
	 	// -- Parse Json from Url to single bean(POJO)
		@SuppressWarnings("rawtypes")
		public T parseJsonUrlToBean(Class <? extends Collection> collectionClass, Class <?> elementClass, String strUrl) {
			logger.info("============ CALL JsonBeanParser -> parseJsonUrlToBean START ===========");
			mapper = new ObjectMapper();
			type = new ObjectMapper().getTypeFactory().constructCollectionType(collectionClass, elementClass);
			mapper = SOUtil.configureFeature(mapper);
			try {
				url = new URL(strUrl);
				list = mapper.readValue(url,type);
				for(T cl : list){
					setT(cl);
				}
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			logger.info("============ CALL JsonBeanParser -> parseJsonUrlToBean END ===========");
			return getT();
		}
		
		// -- Parse Bean List data to Json Object
		public String parseBeanListToJson(List<T> t) throws JsonGenerationException, JsonMappingException, IOException{
			logger.info("============ CALL JsonBeanParser -> parseBeanListToJson START ===========");
			mapper = new ObjectMapper();
			logger.info("============ CALL JsonBeanParser -> parseBeanListToJson END ===========");
			return SOUtil.setDateFormat(mapper).writeValueAsString(t);
		}
		
		// -- Parse Single Bean data to Json Object
		public String parseBeanToJson(T t) throws JsonGenerationException, JsonMappingException, IOException{
			logger.info("============ CALL JsonBeanParser -> parseBeanToJson START ===========");
			mapper = new ObjectMapper();
			logger.info("============ CALL JsonBeanParser -> parseBeanToJson END ===========");
			return SOUtil.setDateFormat(mapper).writeValueAsString(t);
		}
		
		// -- Parse Json List to bean(POJO) List
		public List<T> parseJsonListToBean(Class <? extends Collection> collectionClass, Class <?> elementClass, String jsonString) {
			logger.info("============ CALL JsonBeanParser -> parseJsonListToBean START ===========");
			mapper = new ObjectMapper();
			type = new ObjectMapper().getTypeFactory().constructCollectionType(collectionClass, elementClass);
			mapper = SOUtil.configureFeature(mapper);
//			mapper.setPropertyNamingStrategy(new CStyleStrategy());
			
			try {
				list = mapper.readValue(jsonString,type);
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			logger.info("============ CALL JsonBeanParser -> parseJsonListToBean END ===========");
			return list;
		}
		
		// -- Parse Json to single bean(POJO)
		public T parseJsonToBean(Class <? extends Collection> collectionClass, Class <?> elementClass, String jsonString) {
			logger.info("============ CALL JsonBeanParser -> parseJsonToBean START ===========");
			mapper = new ObjectMapper();
			type = new ObjectMapper().getTypeFactory().constructCollectionType(collectionClass, elementClass);
			mapper = SOUtil.configureFeature(mapper);
			try {
				list = mapper.readValue(jsonString,type);
				for(T cl : list){
					setT(cl);
				}
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			logger.info("============ CALL JsonBeanParser -> parseJsonToBean END ===========");
			return getT();
		}
}
