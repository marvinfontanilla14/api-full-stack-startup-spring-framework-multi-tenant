package com.so.api.dto;

import java.util.ArrayList;
import java.util.List;

public class BeanException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;
	List<String> errors = new ArrayList<>();
	

	public String getErrorMessage() {
		return errorMessage;
	}
	
	public List<String> getErrors() {
		return errors;
	}

	public BeanException(String errorMessage,List<String> errors) {
		super(errorMessage);
		this.errorMessage = errorMessage;
		this.errors = errors;
	}

	public BeanException() {
		super();
	}
	
}
