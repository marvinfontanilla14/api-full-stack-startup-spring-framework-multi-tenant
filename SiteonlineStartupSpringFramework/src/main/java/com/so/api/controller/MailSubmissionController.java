package com.so.api.controller;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mail")
@PropertySource("classpath:mail.properties")
class MailSubmissionController {
//    
//    @Autowired
//    private EmailSenderService emailSenderService;
    
    @Autowired
	private Environment environment;
    
    

//    @RequestMapping("/mail")
//    SimpleMailMessage send() {        
//        SimpleMailMessage mailMessage = new SimpleMailMessage();
//        mailMessage.setTo("marvinfontanilla14@gmail.com");
//        mailMessage.setReplyTo("marvinfontanilla14@gmail.com");
//        mailMessage.setFrom("marvinfontanilla14@gmail.com");
//        mailMessage.setSubject("Lorem ipsum");
//        mailMessage.setText("Lorem ipsum dolor sit amet [...]");
//        javaMailSender.send(mailMessage);
//        return mailMessage;
//    }
    
    @RequestMapping("/mail-template")
    void sendEmailWithTemplate () throws MessagingException  {
//        EmailDetail detail = new EmailDetail();
//        detail.setBooking(bookingService.getBookingById(1));
//        emailSenderService.sendBookingEnrolSuccessEmail(detail, "applicationReceived.vm");
//        emailSenderService.sendBookingManualEnrolEmail(detail, "applicationReceived.vm");
//        emailSenderService.sendBookingInvoiceEmail(detail, "bookingInvoice.vm");
    }
    
    
}
