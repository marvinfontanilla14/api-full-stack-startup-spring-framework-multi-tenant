package com.so.api.controller;
 
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.so.api.domain.Cart;
import com.so.api.service.CartService;
import com.so.api.service.UserService;
 
 
@RestController
public class HelloWorldRestController {
 
    @Autowired 
    CartService cartService;
    
    @Autowired
	private UserService userService;
     
    
    @RequestMapping(value = "/user/", method = RequestMethod.GET)
    public List<Cart> listAllUsers(HttpServletRequest request) {
		String sessionId = "$2a$10$muys59QXR98LxXmzHnAfTerYjqmurrMN2yLvWZw4FPCFt1LkkKWtO";
		List<Cart> cartList = cartService.getCartListBySessionId(sessionId);
		return cartList;
    } 
 
}