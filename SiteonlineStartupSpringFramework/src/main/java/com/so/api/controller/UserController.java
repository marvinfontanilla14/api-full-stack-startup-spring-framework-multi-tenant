package com.so.api.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.so.api.domain.User;
import com.so.api.domain.UserProfile;
import com.so.api.domain.UserReturn;
import com.so.api.dto.BeanException;
import com.so.api.enums.ErrorMessage;
import com.so.api.enums.UserProfileType;
import com.so.api.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public ResponseEntity<UserReturn> createUser(HttpServletRequest request) throws Exception {
		List<String> errors = new ArrayList<>();
		User user = new User();
		
		user.setUsername(request.getParameter("username"));
		user.setPassword(request.getParameter("password"));
		user.setFirstName(request.getParameter("firstName"));
		user.setLastName(request.getParameter("lastName"));
		user.setEmail(request.getParameter("email"));
		user.setTenant(request.getParameter("tenant"));
		String role = request.getParameter("role");
		
		
		if(StringUtils.isEmpty(user.getUsername())) {
			errors.add(ErrorMessage.REQUIRED_USERNAME.getErrorMgs());
		}
		if(StringUtils.isEmpty(user.getPassword())) {
			errors.add(ErrorMessage.REQUIRED_PASSWORD.getErrorMgs());
		}
		if(StringUtils.isEmpty(user.getFirstName())) {
			errors.add(ErrorMessage.REQUIRED_FIRST_NAME.getErrorMgs());
		}
		if(StringUtils.isEmpty(user.getLastName())) {
			errors.add(ErrorMessage.REQUIRED_LAST_NAME.getErrorMgs());
		}
		if(StringUtils.isEmpty(user.getEmail())) {
			errors.add(ErrorMessage.REQUIRED_EMAIL.getErrorMgs());
		}
		if(StringUtils.isEmpty(user.getTenant())) {
			errors.add(ErrorMessage.REQUIRED_TENANT.getErrorMgs());
		}
		
		if(errors.size() > 0) {
			throw new BeanException(ErrorMessage.INVALID_FIELD_VALUE.getErrorMgs(),errors);
		}
		
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		

		if(role != null && role.length() > 0) {
			Set<UserProfile> userProfiles = new HashSet<UserProfile>(); 
			String [] roleArr = role.split(",");
			
			for(String r : roleArr) {
				UserProfile userProfile = new UserProfile();
				if(r.equalsIgnoreCase(UserProfileType.USER.getUserProfileType())) {
					userProfile.setId(UserProfileType.USER.getUserProfileTypeId());
					userProfile.setType(UserProfileType.USER.getUserProfileType());
				}
				if(r.equalsIgnoreCase(UserProfileType.ADMIN.getUserProfileType())) {
					userProfile.setId(UserProfileType.ADMIN.getUserProfileTypeId());
					userProfile.setType(UserProfileType.ADMIN.getUserProfileType());
				}
				if(r.equalsIgnoreCase(UserProfileType.DBA.getUserProfileType())) {
					userProfile.setId(UserProfileType.DBA.getUserProfileTypeId());
					userProfile.setType(UserProfileType.DBA.getUserProfileType());
				}
				userProfiles.add(userProfile);
			}
			
			user.setUserProfiles(userProfiles);
		}

		UserReturn userReturn = new UserReturn();
		userReturn.setId(userService.saveUser(user));
		userReturn.setUsername(user.getUsername());

		return new ResponseEntity<UserReturn>(userReturn, HttpStatus.OK);
	}
}
