package com.so.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.so.api.domain.Employee;
import com.so.api.dto.BeanException;
import com.so.api.dto.ErrorResponse;
import com.so.api.enums.ErrorMessage;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@RequestMapping(value = "/{firstName}", method = RequestMethod.GET)
	public ResponseEntity<List<Employee>> showMessage(
			@PathVariable("firstName") String firstName,
			@RequestParam(value = "empId", required = false, defaultValue = "00000") final String empId) throws Exception {
 
		Employee employee = new Employee();
		employee.setEmpId(empId);
		employee.setName(firstName);
		
		List<String> errors = new ArrayList<>();
		errors.add(ErrorMessage.INVALID_EMAIL.getErrorMgs());
		errors.add(ErrorMessage.INVALID_NUMBER.getErrorMgs());
		if (StringUtils.isNumeric(firstName)) {
			throw new BeanException(ErrorMessage.INVALID_FIELD_VALUE.getErrorMgs(),errors);
		}
		
		List<Employee> listtt = new ArrayList<Employee>();
 
		listtt.add(employee);
		
		return new ResponseEntity<List<Employee>>(listtt, HttpStatus.OK);
	}
	
}
