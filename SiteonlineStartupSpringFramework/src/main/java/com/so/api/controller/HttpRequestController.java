package com.so.api.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/http-request")
public class HttpRequestController {

	@RequestMapping(value = { "/do-request" }, method = RequestMethod.POST)
	public String doPost(){
		return "post action";
	}
	
	@RequestMapping(value = { "/do-request" }, method = RequestMethod.GET)
	public String doGet(){
		return "get action";
	}

	@RequestMapping(value = { "/do-request" }, method = RequestMethod.PUT)
	public String doPut(){
		return "put action";
	}
		
	@RequestMapping(value = { "/do-request" }, method = RequestMethod.DELETE)
	public String doDelete(){
		return "delete action";
	}
	
}
