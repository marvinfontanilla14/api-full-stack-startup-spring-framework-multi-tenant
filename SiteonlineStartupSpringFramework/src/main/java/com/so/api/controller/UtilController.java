package com.so.api.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.so.api.utilities.SOUtil;

@RestController
@RequestMapping("/util")
@PropertySource(value = { "classpath:application.properties" })
public class UtilController {

	private static final Logger logger = Logger.getLogger(UtilController.class);
	
	@RequestMapping(value = { "/encrypt/{txt}" }, method = RequestMethod.GET)
	public String encrypt(@PathVariable String txt){
		logger.info("======== encrypt() Called ========");
		return SOUtil.doEncrypt(txt);
	}
	
	@RequestMapping(value = { "/gen-session" }, method = RequestMethod.GET)
	public String getSession(HttpServletRequest request){
		logger.info("======== getSession() Called ========");
		return request.getSession().getId();
	}
	
}
