package com.so.api.domain;

public class SessionContainer {

	private int id;
	private String val;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getVal() {
		return val;
	}
	public void setVal(String val) {
		this.val = val;
	}
	
	@Override
	public String toString() {
		return "SessionContainer [id=" + id + ", val=" + val + "]";
	}
	
	
}
