-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2016 at 05:21 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `siteonline_tenant`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `instance_id` int(11) DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=292 ;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_id`, `course_id`, `date_created`, `instance_id`, `session_id`, `type`) VALUES
(241, 2211, '2016-08-29 16:06:41', 3312, '$2a$10$muys59QXR98LxXmzHnAfTerYjqmurrMN2yLvWZw4FPCFt1LkkKWtO', 'w'),
(251, 221, '2016-08-29 16:12:20', 3312, '$2a$10$muys59QXR98LxXmzHnAfTerYjqmurrMN2yLvWZw4FPCFt1LkkKWtO', 'w'),
(261, 2214, '2016-08-29 16:12:24', 3312, '$2a$10$muys59QXR98LxXmzHnAfTerYjqmurrMN2yLvWZw4FPCFt1LkkKWtO', 'w'),
(271, 2214, '2016-08-29 16:13:05', 33123, '$2a$10$muys59QXR98LxXmzHnAfTerYjqmurrMN2yLvWZw4FPCFt1LkkKWtO', 'w'),
(281, 22141, '2016-08-29 16:42:38', 33123, '$2a$10$muys59QXR98LxXmzHnAfTerYjqmurrMN2yLvWZw4FPCFt1LkkKWtO', 'w'),
(291, 2222, '2016-08-29 16:43:04', 2222, '$2a$10$muys59QXR98LxXmzHnAfTerYjqmurrMN2yLvWZw4FPCFt1LkkKWtO', 'w');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
